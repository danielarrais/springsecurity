package nivelAcesso;

import java.io.Serializable;
import org.springframework.security.core.GrantedAuthority;

public class NivelAcesso implements Serializable, GrantedAuthority {

    private static final long serialVersionUID = 1683510015743864728L;

    private Integer id;
    private String nivel;
    private String descricao;

    public NivelAcesso() {
    }

    public NivelAcesso(String nivel, String descricao) {
        this.nivel = nivel;
        this.descricao = descricao;
    }

    public NivelAcesso(Integer id, String nivel, String descricao) {
        this.id = id;
        this.nivel = nivel;
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String getAuthority() {
        return nivel;
    }

}
