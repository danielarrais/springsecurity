/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.helowspring.conf;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.AntPathMatcher;

/**
 *
 * @author danie
 */

/**
 * Essa anottations deve ser utilizadas nas classe de configuração do spring,
 * pois ele faz com que componentes importante sejam carregados
 */
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    
//  private static final String USUARIO_POR_LOGIN = "select user_name, password, enabled from users where user_name = ?";
	
//  private static final String PERMISSOES_POR_USUARIO = "select u.user_name, ur.authority from users u, user_roles ur where u.user_id = ur.user_id and u.user_name = ? ";

  private static final String USUARIO_POR_LOGIN = "select email, senha, ativo from usuario where email = ?";
	
  private static final String PERMISSOES_POR_USUARIO = "select u.email, n.nivel from usuario u, nivel_usuario ur inner join nivelacesso n on n.id_nivel  = ur.id_nivel\n" +
"where u.id_usuario = ur.id_usuario and u.email = ? ";
    
    @Autowired
    private UserDetailsService users;
    
    @Autowired
    DataSource dataSource;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/admin").hasRole("ADMIN")
                .antMatchers("/user").hasRole("USER")
                .antMatchers("/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/login").permitAll().and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout")).logoutSuccessUrl("/");
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(users);
    }
//    
//    @Override
//  protected void configure(AuthenticationManagerBuilder builder) throws Exception {
//    builder
//        .jdbcAuthentication()
//        .dataSource(dataSource)
//        .usersByUsernameQuery(USUARIO_POR_LOGIN)
//        .authoritiesByUsernameQuery(PERMISSOES_POR_USUARIO);
//  }
}