package org.helowspring.conf;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * Essa classe faz com que o Spring MVC reconheça outras classes
 */
public class ServletSpringMVC extends AbstractAnnotationConfigDispatcherServletInitializer {
	/**
	 * Método onde devem ser passadas as classes que serão carregadas, ou que dirão
	 * quais deverão ser carregadas. Esse, diferente do segundo, carrega as classe
	 * logo depois de ser carregado no servidor
	 */
	@Override
	protected Class<?>[] getRootConfigClasses() {
		return new Class[] { SecurityConfiguration.class, AppWebConfiguration.class, MYSQLConfiguration.class };
	}

	/**
	 * Método onde devem ser passadas as classes que serão carregadas, ou que dirão
	 * quais deverão ser carregadas
	 */
	@Override
	protected Class<?>[] getServletConfigClasses() {
		return new Class[] {};
	}

	/**/
	@Override
	protected String[] getServletMappings() {
		return new String[] { "/" };
	}
}