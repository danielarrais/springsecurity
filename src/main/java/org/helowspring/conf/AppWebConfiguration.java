package org.helowspring.conf;

import nivelAcesso.NivelAcessoDAOMysql;
import org.helowspring.controllers.UrlsControllers;
import usuario.UsuarioDAOMysql;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
/**
 * A anotação @EnableWebMvc habilita o Spring MVC
 */
@EnableWebMvc
/**
 * A anotação componente scan, será responsável por dizer pro spring
 * quais pacote deverão ser escaneados.
 * No caso, foram passadas uma classe de cada pacote, em vez do nome do pacote.
 */
@ComponentScan(basePackageClasses = {UrlsControllers.class, UsuarioDAOMysql.class, NivelAcessoDAOMysql.class})
public class AppWebConfiguration {
	/**
	 * A classe InternalResourceViewResolver guarda as informações da pasta base do sistema
	 */
	@Bean
	public InternalResourceViewResolver internalResourceViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
}
