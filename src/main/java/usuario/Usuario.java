package usuario;

import nivelAcesso.NivelAcesso;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class Usuario implements Serializable, UserDetails {

    private static final long serialVersionUID = 1991432369109352952L;
    private Integer id;
    private String name;
    private String sobrenome;
    private String email;
    private String emailSec;
    private String senha;
    private Date nascimento;
    private List<NivelAcesso> roles = new ArrayList<NivelAcesso>();

    public Usuario() {
    }

    public Usuario(String nome, String sobrenome, String email, String emailSec, String senha, Date nascimento, ArrayList<NivelAcesso> nivelAcesso) {
        this.name = nome;
        this.sobrenome = sobrenome;
        this.email = email;
        this.emailSec = emailSec;
        this.senha = senha;
        this.nascimento = nascimento;
        this.roles = nivelAcesso;
    }

    public Usuario(Integer id, String nome, String sobrenome, String email, String emailSec, String senha, Date nascimento, ArrayList<NivelAcesso> nivelAcesso) {
        this.id = id;
        this.name = nome;
        this.sobrenome = sobrenome;
        this.email = email;
        this.emailSec = emailSec;
        this.senha = senha;
        this.nascimento = nascimento;
        this.roles = nivelAcesso;
    }

    public List<NivelAcesso> getRoles() {
        return roles;
    }

    public void setRoles(List<NivelAcesso> roles) {

        this.roles = roles;
    }
    public void addNivelAcesso(NivelAcesso nivelAcesso) {
        if (this.roles == null) {
            this.roles = new ArrayList<NivelAcesso>();
        }
        this.roles.add(nivelAcesso);
    }
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailSec() {
        return emailSec;
    }

    public void setEmailSec(String emailSec) {
        this.emailSec = emailSec;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return senha;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

}
