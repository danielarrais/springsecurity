<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <title>Login Page</title>
    </head>
    <body>
        <h3>Informe seu email e sua senha para fazer login</h3>
        <form:form servletRelativeAction="/login">
            <table>
                <tr>
                    <td>Email:</td>
                    <td><input type='text' name='username' value=''></td>
                </tr>
                <tr>
                    <td>Senha:</td>
                    <td><input type='password' name='password' /></td>
                </tr>
                <tr>
                    <td colspan='2'><input name="submit" type="submit" value="Login" /></td>
                </tr>
            </table>
            <input type="hidden" name="${_csrf.parameterName }" value="${_csrf.token }"/>
        </form:form>
    <c:set var="contextPath" value="${pageContext.request.contextPath}" />
    <a href="${contextPath}/">P�gina Inicial</a>
</body>
</html>