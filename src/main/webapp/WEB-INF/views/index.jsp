<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Insert title here</title>
    </head>
    <body>
        <security:authorize  access="isAuthenticated()">
		<security:authentication property="principal" var="user" />
		<h3>Usu�rio logado: ${user}</h3>
	</security:authorize>
        <c:set var="contextPath" value="${pageContext.request.contextPath}" />
        <h1>Seja bem vindo ao Spring Security + Spring MVC</h1>
        Clique	<a href="${contextPath}/admin">aqui</a> para acessar a administra��o
        <br /> Clique <a href="${contextPath}/user">aqui</a> para acessar a �rea do usu�rio
        <br/><br/>
        <a href="${contextPath}/logout">Sair</a>
    </body>
</html>