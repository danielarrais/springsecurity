<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <title>JSP Page</title>
    </head>
    <body>
        <form method="post" action="/helowspring/produtos">
            <div>
                <label for="title">Titulo</label>
                <input type="text" name="title" id="title"/>
            </div>
            <div>
                <label for="description">Descri��o</label>
                <textarea cols="20" type="text" name="description" id="description"></textarea>
            </div>
            <div>
                <label for="pages">N�mero de p�ginas</label>
                <input type="text" name="pages" id="pages"/>
            </div>
            <div>
                <input type="submit" value="Enviar" />
            </div>
            <c:forEach items="${types}" var="bookType" varStatus="status">
                <div>
                    <label for="price_${bookType}">${bookType}</label>
                    <input type="text" name="prices[${status.index}].value" id="price_${bookType}"/>
                    <input type="hidden" name="prices[${status.index}].bookType" value="${bookType}"/>
                </div>
            </c:forEach>
        </form>
    </body>
</html>