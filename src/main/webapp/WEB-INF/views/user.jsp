<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>P�gina do Usu�rio</title>
</head>
<body>
	<security:authentication property="principal" var="user" />
	<h1>${user} seja bem vindo ao sistema!</h1>
        <c:set var="contextPath" value="${pageContext.request.contextPath}" />
        <a href="${contextPath}/">P�gina Inicial</a>
        <a href="${contextPath}/logout">Sair</a>
        
</body>
</html>